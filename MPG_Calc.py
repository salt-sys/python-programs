# Start of Header
# Andrew Aurand January 17, 2021
# Programming Assignment Two: Miles Per Gallon Calculator
# End of Header

#Introduction and purpose of program
print("Fuel Cost Calculator By Andrew Aurand")
print("""This program will help you determine your total cost of fuel per month. 
To use it, you will be asked about your car's MPG (Miles per Gallon), total amount driven per month, and the cost of fuel in your area.""")
print("This program will then calculate the monthly fuel cost")

input("press any key to continue")

#prompts the user to input thier car's miles per gallon and converts input to a float
car_mpg = input("Enter your car's Miles Per Gallon:")
convertmpg = float(car_mpg)
#prompts the user to input thier estimated miles driven per month and converts input to a float
avg_driven_month = input("Enter the average number of miles driven per month:")
convert_avg_driven_month = float(avg_driven_month)
#prompts the user for local fuel prices and converts input to a float
fuel_price = input("Enter the cost of fuel per gallon:")
convert_fuel_price = float(fuel_price)

gallon_used_monthly = convert_avg_driven_month / convertmpg
monthly_fuel_cost = gallon_used_monthly * convert_fuel_price
#reads back fuel price and average miles driven
print("Given the price of fuel at ${:.2f}".format(convert_fuel_price)+" " + "and {:.2f}".format(convert_avg_driven_month) + " " + "miles driven per month")
print(" ")
print("Gallons used each month:" '{:,.1f}'.format (gallon_used_monthly))
print("Monthly Cost of Fuel:"  '${:,.2f}'.format (monthly_fuel_cost))


