# Start of Header File
# Author: Andrew Aurand --- Date of Creation: 2/1/2021
# Program Title: Programming Assignment 4 (Menu program) Version 1.0
# End of Header

#option selection
Guest_List = ['Andrew' , 'Sasha' , 'Tom']
selection_menu = """ 
Press 1 to display a sorted list of guests:
Press 2 to add a guest to the list
Press 3 to delete a guest from the list
Press 0 to terminate the program """

done = False
#Menu start
while not done:
    print(selection_menu)
    selection = input("Make a selection:")
    print()
    
    if selection == "0":
        done = True
    #Displays guest list
    elif selection == "1":
        print()
        print("Guest List")
        print("-" * 16)
        for item in (sorted(Guest_List)):
            print (item)
    #Add a user to the guest list, checks if name entered exists
    elif selection == "2":
        Add_Guest = input ("Please enter a name to add to the list:").title()
        if Add_Guest in Guest_List:
            print("This name already exists in the Guest List")
        else:
            Guest_List.append(Add_Guest)
            print ("Guest added successfully")
        
    #Deletes a user, checks that a valid user name was entered, uses try/except so the program won't terminate.
    elif selection == "3":
        try:
           Del_Guest = input ("Please enter a name to delete from the list:").title()
           Guest_List.remove(Del_Guest)
           print ("Guest deleted successfully")
        except:
          print ("You entered a name not on the list, please try inputting the name you want to delete again")
    else:
       print (" Press selection option 0, 1, 2, or 3!")
