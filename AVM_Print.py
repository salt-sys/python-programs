import sys #for terminating the program
#week 1 homework - Andrew Aurand

# Exercise 1 Animal, Vegetable, Mineral

#introduction prompt
print("listed are the homework exercises in order")
input("press any key to continue onto exercise one")

#defining variables
animal = 'kitten'
vegetable = 'potato'
mineral = 'iron'

#printing variables
print("Here is an animal, a vegetable, and a mineral")
print (animal)
print (vegetable)
print (mineral)
#end of exercise one

input("press any key to move on to exercise two")

#exercise 2: Copy Cat
print("welcome to exercise two")
#Define user input as a var, then take user input
user_input = input("Please type something and press enter:")
print("you entered:")
#Displays whatever the user inputted.
print(user_input)
#end of exercise two

input("press any key to move on to exercise three")

#exercise 3: what does the cat say?

text = input("what would you like the cat to say?")
text_length = len(text)
print('         {}'.format('_' * text_length))
print('         <{}'.format(text))
print('        / {}'.format('-' * text_length))
print('       /')
print(' /\_/\/ ')
print(' > ^ < ')

#end of exercise three

input("press any key to move onto supplemental exercises")

#supplemental exercise 1: concatenating strings
#xkcd reference: https://xkcd.com/936/ 
word1 = 'correct'
word2 = 'horse'
word3 = 'battery'
word4 = 'stapler'
#print the vars listed above, and put a space between each word.
print("here are four common words from a famous web comic:")
print(word1 + " " + word2 + " " + word3 + " " + word4)
#end of supplemental exercise one

input("press any key to move on to supplemental exercise two")

#supplemental exercise 2: simple printing
team = "Detroit Red Wings"
status = "rocks"

punctuation = "!"

print(team, status, punctuation)


#prompts the user to exit by calling sys.exit
input ("press any key to close the program")
sys.exit()




